/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora.comandos;

/**
 *
 * @author Victor
 */
public class CalculadoraComando {
    char codOperacao;
    String resultadoOp;
    public String multiplicacao(String op1, String op2){
        resultadoOp = "";
        if(op1 != null)
            resultadoOp += (Float.parseFloat(op1) * Float.parseFloat(op2));
        else
            resultadoOp += Float.parseFloat(op2) * Float.parseFloat(op2);    
        return resultadoOp;
    }
    
    public String divisao(String op1, String op2){
        resultadoOp = "";    
        if(Float.parseFloat(op2) != 0){
        if(op1 != null)
            resultadoOp += (Float.parseFloat(op1) / Float.parseFloat(op2));
        else
            resultadoOp += Float.parseFloat(op2) / Float.parseFloat(op2);    
        }else
                resultadoOp = "Não é possível dividir por zero!";
        return resultadoOp;
    }
    
    public String soma(String op1, String op2){
        resultadoOp = "";
        if(op1 != null)
            resultadoOp += (Float.parseFloat(op1) + Float.parseFloat(op2));
        else
            resultadoOp += Float.parseFloat(op2) + Float.parseFloat(op2);  
        return resultadoOp;
    }
    
    public String subtracao(String op1, String op2){
        resultadoOp = "";
        if(op1 != null)
            resultadoOp += (Float.parseFloat(op1) - Float.parseFloat(op2));
        else
            resultadoOp += Float.parseFloat(op2) - Float.parseFloat(op2);  
        return resultadoOp;
    }
    public boolean eOperacao(String op){
        boolean validacao = false;
        switch (op) {
            case "+" :
                validacao = true;
                break;
            case "-" :
                validacao = true;
                break;
            case "*" :
                validacao = true;
                break;
            case "/" :
                validacao = true;
                break;
            default :
                validacao = false;
        }
        return validacao;
    }
    public String criaExpressao(String op1, String op2, String op, String resultado){
        String expressao = "";       
        op1 = op1 != null ? op1 : op2;
        expressao = op1 + " " + op + " " + op2 + " = " + resultado;
        return expressao;
    }
}
